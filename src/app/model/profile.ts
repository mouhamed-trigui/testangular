export class Profile {
    id: string;
    fullName: string;
    empCode: string;
    position: string;
    mobile: string;
}
